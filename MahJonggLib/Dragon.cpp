#include "Dragon.h"

using namespace std;

namespace MahJonggLib
{	
	bool operator==( const Dragon& a, const Dragon& b)
	{
		return a.value_ == b.value_;
	}

	bool operator<( const Dragon& a, const Dragon& b)
	{
		return a.value_ < b.value_;
	}

	std::string Dragon::Name() const
	{
		switch(value_)
		{
		case Red_:
			return "Red dragon";
		case Green_:
			return "Green dragon";
		case White_:
			return "White dragon";

		default:
			throw std::range_error("Invalid Dragon!");
		}
	}

	Honour_base::HonourTypes  Dragon::DragonTypes()
	{
		Honour_base::HonourTypes  dragons;
		dragons.push_back(Dragon::Red());
		dragons.push_back(Dragon::Green());
		dragons.push_back(Dragon::White());
		return dragons;
	}

}