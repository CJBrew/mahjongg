#pragma once
#include "Tile.h"

namespace MahJonggLib
{
class TileSet
{
	Tiles tiles_;
	TileSet(Tiles::const_iterator tilesBegin, Tiles::const_iterator tilesEnd) : tiles_(tilesBegin, tilesEnd)
	{}
public:		
	
	Tiles::const_iterator begin() const { return tiles_.begin(); }
	Tiles::const_iterator end() const { return tiles_.end(); }

	static TileSet CreateTileset_136();
};	
}