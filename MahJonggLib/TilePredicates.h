#pragma once

#include "Tile.h"
#include "Suit.h"
#include "Honour.h"

namespace MahJonggLib
{
	struct IsSameHonour
	{
	private:
		Honour h_;
	public:
		IsSameHonour(Honour h): h_(h)
		{}

		bool operator()(const pTile& pT)
		{
			if(pT->IsHonourTile())
			{
				return AsHonourTile(pT)->GetHonour() == h_;
			}
			else return false;
		}
	};


	struct IsSameSuit
	{
	private:
		Suit s_;
	public:
		IsSameSuit(Suit s): s_(s)
		{}

		bool operator()(const pTile& pT)
		{
			if(pT->IsSuitTile())
			{
				return AsSuitTile(pT)->GetSuit() == s_;
			}
			else return false;
		}
	};
	
	static bool IsSameNumber(SuitTile a, SuitTile b)
	{
		 return a.GetNumber() == b.GetNumber();
	}


}