#pragma once

#include "HonourBase.h"
#include <string>
#include <vector>

namespace MahJonggLib
{
	struct Dragon : public Honour_base
	{
	private:
		enum Dragons_ { Red_, Green_, White_ };

		Dragons_ value_;

	public:		
		explicit Dragon(Dragons_ value) : value_(value)
		{}		

		static HonourType Red()
		{
			return HonourType(new Dragon(Red_));
		}

		static HonourType Green()
		{
			return HonourType(new Dragon(Green_));
		}

		static HonourType White()
		{
			return HonourType(new Dragon(White_));
		}

	public:		
		std::string Name() const;

		static Honour_base::HonourTypes  DragonTypes();

		friend bool operator<( const Dragon& a, const Dragon& b);
		friend bool operator==( const Dragon& a, const Dragon& b);
	};		

}

