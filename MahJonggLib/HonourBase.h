#pragma once

#include <string>
#include <vector> 
#include <memory>

namespace MahJonggLib
{
	struct Honour_base
	{
		typedef std::tr1::shared_ptr<Honour_base> HonourType;
		typedef std::vector<HonourType> HonourTypes;
	private:			
	public:		
		virtual std::string Name() const = 0;

		static HonourTypes GetHonourTypes();
	};		

}

