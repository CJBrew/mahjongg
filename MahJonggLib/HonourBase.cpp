#include "HonourBase.h"

#include <algorithm>

#include "Wind.h"
#include "Dragon.h"

using namespace std;

namespace MahJonggLib
{	
	Honour_base::HonourTypes Honour_base::GetHonourTypes()
	{
		Honour_base::HonourTypes honourTypes;
		const Honour_base::HonourTypes windTypes = Wind::WindTypes();
		std::copy(windTypes.begin(), windTypes.end(), back_inserter(honourTypes));
		const Honour_base::HonourTypes dragonTypes = Dragon::DragonTypes();
		std::copy(dragonTypes.begin(), dragonTypes.end(), back_inserter(honourTypes));
		return honourTypes;	
	}

}