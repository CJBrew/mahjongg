#pragma once 

#include "Tile.h"
#include "Bonus.h"
#include <boost\noncopyable.hpp>

namespace MahJonggLib
{
		class BonusTile : public Tile
	{
		Bonus bonus_;		

		BonusTile(const Bonus& bonus) : Tile(Bonus::Name(bonus)), bonus_(bonus)
		{
		}

		static void CheckNumber(int);
		static std::string GetName(const Bonus&  bonus);
	public:
		bool IsSuitTile() { return false; }
		bool IsHonourTile() { return true; }
		Bonus GetBonus() const { return bonus_; }
		static pTile Create(const Bonus&  bonus);
	};

}
