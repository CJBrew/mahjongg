#pragma once
#include "Iterators.h"

namespace MahJonggLib
{
	typedef std::vector<pTile> Run;
	typedef std::vector<Run> Runs;
	
	void FindRuns(IteratorPairs::const_iterator begin, IteratorPairs::const_iterator end, 
						std::back_insert_iterator<Runs>);

	bool IsChow(const Run&);
}