#include "HandEvaluation.h"

#include <algorithm>

#include "Tile.h"
#include "SuitTile.h"
#include "FindPairs.h"

#include "Meld.h"
#include "Run.h"

using namespace std;
using namespace MahJonggLib;

namespace
{	
	bool AreEqual(const pTile& a, const pTile& b)
	{
		return a == b;
	}
}

namespace MahJonggLib
{
	HandEvaluation::HandEvaluation(const Hand& hand) : tiles_(hand.begin(), hand.end())
	{			
		FindPairs(tiles_.begin(), tiles_.end(), 
			AreConsecutiveSuitTiles, 
			back_inserter<IteratorPairs>(consecutivePairs_));

		FindPairs(tiles_.begin(), tiles_.end(), 
			AreEqual, 
			back_inserter<IteratorPairs>(matchingPairs_));

		FindMelds(matchingPairs_.begin(), matchingPairs_.end(), back_inserter(possibleMelds_));
		FindRuns(consecutivePairs_.begin(), consecutivePairs_.end(), back_inserter(possibleRuns_));
	}

	HandEvalContentS HandEvaluation::CalculatePossibleContents() const
	{
		HandEvalContentS hecs;
		return hecs;
	}

	bool HandEvaluation::IsMahJongg() const
	{
		// copy the set of tiles, remove from the copy those tiles we identify as sets
		//IteratorPair unMatched;(tiles_.begin(), tiles_.end());
		Tiles matched;

		for(Melds::const_iterator it = possibleMelds_.begin(); it != possibleMelds_.end(); ++it)
		{
			CheckMeld(*it);

			copy(it->begin(), it->end(), back_inserter(matched));
			//for(Hand::HandContentsIterator tileIt = it->first; tileIt <= it->second; ++tileIt)
			//{
			//	matched.push_back(*tileIt);
			//}
			//copy(it->first, it->second, back_inserter(matched));
			//	matched.push_back(*it);
		}		

		int nRuns = count_if(possibleRuns_.begin(), possibleRuns_.end(), IsChow);
		int nMelds = count_if(possibleMelds_.begin(), possibleMelds_.end(), IsPung);
		nMelds += count_if(possibleMelds_.begin(), possibleMelds_.end(), IsKong);
		int nPairs = matchingPairs_.size();

		if((nRuns + nMelds) == 4 && nPairs == 1) 
		{
			return true;
		}
		return false;
	}
}
