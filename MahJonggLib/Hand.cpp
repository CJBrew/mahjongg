#include "Hand.h"
#include <algorithm>

using namespace std;
namespace MahJonggLib
{
	void Hand::Discard(const pTile& pT)
	{
		HandContents::iterator it = find(tiles_.begin(), tiles_.end(), pT);
		if(tiles_.end() != it)
		{
			tiles_.erase(it);
		}	

		sort(tiles_.begin(), tiles_.end());
	}

	void Hand::Draw(const pTile& pT)
	{		
		tiles_.push_back(pT);

		sort(tiles_.begin(), tiles_.end());
	}
}