#include "Suit.h"

namespace MahJonggLib
{	
	std::string Suit::Name(const Suit suit)
	{
		switch(suit.value_)
		{
		case Coins_:
			return "Coins";
		case Characters_:
			return "Characters";
		case Bamboo_:
			return "Bamboo";
		default:
			throw std::range_error("Invalid Suit!");
		}
	}
	
	std::vector<Suit> Suit::SuitTypes()
	{
		Suit Suits[] = { Suit::Bamboo(), 
			Suit::Characters(), 
			Suit::Coins() };
		return std::vector<Suit>(Suits, Suits + 3);
	}
	
	bool operator==( const Suit& a, const Suit& b)
	{
		return a.value_ == b.value_;
	}

	bool operator<( const Suit& a, const Suit& b)
	{
		return a.value_ < b.value_;
	}

}