#pragma once

#include <string>
#include <vector>

namespace MahJonggLib
{
	struct Honour
	{
	private:
		enum Honour_ { East_, South_, West_, North_, Red_, Green_, White_ };

		Honour_ value_;

	public:		
		explicit Honour(Honour_ value) : value_(value)
		{}

		static Honour EastWind()
		{
			return Honour(East_);
		}
		static Honour SouthWind()		
		{
			return Honour(South_);
		}
		static Honour WestWind()
		{
			return Honour(West_);
		}
		static Honour NorthWind()
		{
			return Honour(North_);
		}
		static Honour RedDragon()
		{
			return Honour(Red_);
		}
		static Honour GreenDragon()
		{
			return Honour(Green_);
		}
		static Honour WhiteDragon()
		{
			return Honour(White_);
		}

		static std::string Name(const Honour);

		static std::vector<Honour> HonourTypes();

		friend bool operator<( const Honour& a, const Honour& b);
		friend bool operator==( const Honour& a, const Honour& b);
	};		

}

