#pragma once 

#include "Tile.h"
#include "Honour.h"
#include <memory>
#include <boost\noncopyable.hpp>

namespace MahJonggLib
{
	class HonourTile : public Tile
	{
		Honour honour_;		

		HonourTile(const Honour& honour) : Tile(Honour::Name(honour)), honour_(honour)
		{
		}

		static void CheckNumber(int);
		static std::string GetName(const Honour&  honour);
	public:
		bool IsSuitTile() { return false; }
		bool IsHonourTile() { return true; }
		Honour GetHonour() const { return honour_; }
		static pTile Create(const Honour&  honour);
	};
}