#include "Run.h"

#include <cassert>

#include "SuitTile.h"

using namespace MahJonggLib;

namespace
{	
	bool PairsMakeRun(IteratorPair itPair, IteratorPair itPair2)
	{
		if(1 == distance(itPair.first, itPair2.first))
		{
			bool res = AreConsecutiveSuitTiles(*itPair.first, *itPair2.first);
			return res;
		}
		return false;
	}
}

namespace MahJonggLib
{
	void FindRuns(IteratorPairs::const_iterator begin, IteratorPairs::const_iterator end, 
						std::back_insert_iterator<Runs> runs)
	{		
		IteratorPairs::const_iterator start = begin;
		while(start != end)
		{
			IteratorPairs::const_iterator found = std::adjacent_find(start, end, PairsMakeRun);
			if(found == end)
			{
				return;
			}
			else
			{
				IteratorPairs::const_iterator next = found + 1;
				Run run(found->first, next->second);
				runs = run;
			}
			start = found + 1;
		}
		return;
	}
	
	bool IsChow(const Run& run)
	{
		try
		{
			assert(run.size() == 3);

			SuitTile* a = AsSuitTile(run[0]);
			SuitTile* b = AsSuitTile(run[2]);
			bool res = b->GetNumber() - a->GetNumber() == 2; //CJY crap!
			return res;
		}
		catch(const std::bad_cast&)
		{
			return false;
		}
	}
}
