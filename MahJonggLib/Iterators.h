#pragma once

#include <vector>
#include <algorithm>
#include "Hand.h"

namespace MahJonggLib
{
	typedef std::pair<Hand::HandContentsIterator, Hand::HandContentsIterator> IteratorPair;

	inline int CalculateDistance(const IteratorPair& pair)
	{
		return std::distance(pair.first, pair.second);
	}

	typedef std::vector<IteratorPair> IteratorPairs;
}