#pragma once

#include <string>
#include <vector>

namespace MahJonggLib
{
	struct Honour
	{
	private:
		enum Honour_ { Wind_, Dragon_ }; //East_, South_, West_, North_, Red_, Green_, White_ };

		Honour_ value_;

	public:		
		explicit Honour(Honour_ value) : value_(value)
		{}

	public:		
		std::string Name() const;

		static std::vector<Honour> HonourTypes();

		friend bool operator<( const Honour& a, const Honour& b);
		friend bool operator==( const Honour& a, const Honour& b);
	};		

	struct Wind
		{
		private:
			enum Wind_ { East_, South_, West_, North_ };

			Wind_ value_;
		public:
			explicit Wind(Wind_ value) : value_(value)
			{}

		// private:
			
			static Wind East()
			{
				return Wind(East_);
			}
			static Wind South()		
			{
				return Wind(South_);
			}
			static Wind West()
			{
				return Wind(West_);
			}
			static Wind North()
			{
				return Wind(North_);
			}
					std::string Name() const;

		};

		struct Dragon: public Honour
		{
		private:
			enum Dragon_ { Red_, Green_, White_ };

			Dragon_ dragon_;
		public:
			explicit Dragon(Dragon_ value) : value_(value)
			{}

//		private:		

			static Dragon Red()
			{
				return Dragon(Red_);
			}
			static Dragon Green()
			{
				return Dragon(Green_);
			}
			static Dragon White()
			{
				return Dragon(White_);
			}

			std::string Name() const;					
		};

}

