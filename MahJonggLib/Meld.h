#pragma once

#include "Iterators.h"
#include <vector>
#include "tile.h"

namespace MahJonggLib
{	
	typedef std::vector<pTile> Meld;
	typedef std::vector<Meld> Melds;
	
	void FindMelds(IteratorPairs::const_iterator begin, IteratorPairs::const_iterator end, 
						std::back_insert_iterator<Melds>);
	
	void CheckMeld(const Meld&);

	//bool IsPair(const Meld&);
	bool IsPung(const Meld&);
	bool IsKong(const Meld&);
}