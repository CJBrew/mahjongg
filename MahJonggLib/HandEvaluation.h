#pragma once

#include "Hand.h"
#include "Iterators.h"
#include "Meld.h"
#include "Run.h"
#include <vector>

namespace MahJonggLib
{		
	typedef std::vector<pTile> Pair;
	typedef std::vector<Pair> Pairs;

	bool IsChow(const Run& run);

	bool IsPung(const Meld& m);
	bool IsKong(const Meld& m);

	bool IsLimitHand(const Hand& hand);

	struct HandEvalContent
	{
		Pairs pairs_;
		Melds melds_;
		Runs runs_;
	};

	typedef std::vector<HandEvalContent> HandEvalContentS;

	class HandEvaluation
	{	
	private:
		Tiles tiles_;
		IteratorPairs matchingPairs_; 
		IteratorPairs consecutivePairs_;
		Melds possibleMelds_;
		Runs possibleRuns_;
	public:
		explicit HandEvaluation(const Hand&);

		HandEvalContentS CalculatePossibleContents() const;

		bool IsMahJongg() const;
	};
}