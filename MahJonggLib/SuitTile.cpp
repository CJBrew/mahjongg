#include "SuitTile.h"
#include "Suit.h"
#include <boost\format.hpp>
#include <string>

using namespace std;
using namespace boost;

namespace MahJonggLib
{	
	void SuitTile::CheckNumber(int number)
	{
		if(number < 1 || number > 9)
		{
			throw std::range_error("Tile number out of range");
		}
	}

	string SuitTile::GetName(Suit suit, int number)
	{
		CheckNumber(number);

		return (format("%1% %2%") % number % Suit::Name(suit)).str();
	}

	pTile SuitTile::Create(Suit suit, int number)
	{
		return pTile(new SuitTile(suit, number));
	}
		
	bool AreConsecutiveSuitTiles(const pTile& a, const pTile& b)
	{
		if(a->IsSuitTile() && b->IsSuitTile())
		{
			SuitTile* as = AsSuitTile(a);
			SuitTile* bs = AsSuitTile(b);
			if(as->GetSuit() == bs->GetSuit())
			{
				return bs->GetNumber() - as->GetNumber() == 1;
			}
		}

		return false;
	}
}