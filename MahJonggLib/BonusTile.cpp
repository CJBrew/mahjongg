#include "BonusTile.h"

namespace MahJonggLib
{
	std::string BonusTile::GetName(const Bonus& bonus)
	{
		return bonus.Name(bonus);
	}

	pTile BonusTile::Create(const Bonus& bonus)
	{
		return pTile(new BonusTile(bonus));
	}
}