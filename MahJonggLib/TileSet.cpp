#include "TileSet.h"

#include <algorithm>
#include <functional>

#include "Tile.h"
#include "SuitTile.h"
#include "HonourTile.h"

using namespace std;
using namespace std::tr1;
using namespace std::tr1::placeholders;

namespace MahJonggLib
{	
	TileSet TileSet::CreateTileset_136()
	{
		Tiles tiles;
		
		const vector<Suit>& suitTypes = Suit::SuitTypes();
		const vector<Honour>& honourTypes = Honour::HonourTypes();
				
		for(int i = 0; i < 4; ++i)
		{						
			transform(honourTypes.begin(), honourTypes.end(), back_inserter(tiles), HonourTile::Create);
			
			for(int j = 1; j <= 9; ++j)
			{
				transform(suitTypes.begin(), suitTypes.end(), back_inserter(tiles), bind(SuitTile::Create, _1, j));				
			}
		}

		return TileSet(tiles.begin(), tiles.end());
	}
}