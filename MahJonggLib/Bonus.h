#pragma once

#include <string>

namespace MahJonggLib
{
	struct Bonus
	{
	private:
		enum Bonus_ { Spring_, Summer_, Autumn_, Winter_, Plum_, Chrysanthemum_, Orchid_, Bamboo_ };

		Bonus_ value_;

	public:		
		explicit Bonus(Bonus_ value) : value_(value)
		{}

		static Bonus Spring()
		{
			return Bonus(Spring_);
		}
		static Bonus Summer()		
		{
			return Bonus(Summer_);
		}
		static Bonus Autumn()
		{
			return Bonus(Autumn_);
		}
		static Bonus Winter()
		{
			return Bonus(Winter_);
		}
		static Bonus Plum()
		{
			return Bonus(Plum_);
		}
		static Bonus Chrysanthemum()
		{
			return Bonus(Chrysanthemum_);
		}
		static Bonus Orchid()
		{
			return Bonus(Orchid_);
		}
		static Bonus Bamboo()
		{
			return Bonus(Bamboo_);
		}

		bool operator<(Bonus const& other )    
		{ 
			return value_ < other.value_; 
		}

		bool operator==( Bonus const& other )     
		{ 
			return value_ == other.value_; 
		}

		static std::string Name(const Bonus);
	};
}