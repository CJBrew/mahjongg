#pragma once

#include <deque>

#include "Tile.h"
#include "TileSet.h"

namespace MahJonggLib
{
	class Wall
	{
		std::deque<pTile> wall_;

		void Shuffle();
	public:
		explicit Wall(const TileSet& tiles);

		bool HasTiles();
		pTile GetNext();
		pTile GetLoose();
	};
}
