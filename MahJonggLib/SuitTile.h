#pragma once 

#include "Tile.h"
#include "Suit.h"
#include <memory>
#include <boost\noncopyable.hpp>

namespace MahJonggLib
{
	class SuitTile : public Tile
	{
		Suit suit_;
		int number_;

		SuitTile(const Suit& suit, int number) : Tile(GetName(suit, number)), suit_(suit), number_(number)
		{
		}

		static void CheckNumber(int);	
		static std::string GetName(Suit suit, int number);
	public:
		bool IsSuitTile() { return true; }
		bool IsHonourTile() { return false; }
		Suit GetSuit() const { return suit_; }
		int GetNumber() const { return number_; }
		static pTile Create(Suit suit, int number);
	};

	bool AreConsecutiveSuitTiles(const pTile& a, const pTile& b);
}