#include "Meld.h"

using namespace MahJonggLib;

namespace 
{
	bool AllTilesEqual(IteratorPair itPair, IteratorPair itPair2)
	{
		for(Hand::HandContentsIterator it = itPair.first;
			it <= itPair2.second;
			++it)
		{
			// if it's not the same as itPair.first, that sucks
			if(*itPair.first == *it)
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		return true;
	}
}

namespace MahJonggLib
{
	void FindMelds(IteratorPairs::const_iterator begin, IteratorPairs::const_iterator end, 
						std::back_insert_iterator<Melds> melds)
	{
		IteratorPairs::const_iterator start = begin;
		while(start != end)
		{
			IteratorPairs::const_iterator found = std::adjacent_find(start, end, AllTilesEqual);
			if(found == end)
			{
				return;
			}
			else
			{
				IteratorPairs::const_iterator next = found + 1;
				Meld meld(found->first, next->second);
				melds = meld;
			}
			start = found + 1;
		}

		return;
	}

	void CheckMeld(const Meld& m)
	{
		const int meldSize = m.size();
		if(meldSize < 3 || meldSize > 4)
		{
			throw std::exception("Invalid Meld");
		}
		
		Meld::const_iterator found = std::find_if(m.begin(), m.end(), std::bind2nd(std::not_equal_to<pTile>(), *m.begin()));
		if(found != m.end())
		{
			throw std::exception("Invalid Meld");
		}
	}

	//bool IsPair(const Meld& m)
	//{
	//	//CheckMeld(m);
	//	return CalculateDistance(m) == 0;
	//}
	bool IsPung(const Meld& m)
	{
		throw std::exception("IsPung not implemented or tested!");
	}
	//	return m.size() == 3;
	//	///CheckMeld(m);
	//	//return CalculateDistance(m) == 1;
	//}	
	bool IsKong(const Meld& m)
	{
		throw std::exception("IsKong not implemented or tested!");
	}
	//{
	//	return m.size() == 4;
	//	//CheckMeld(m);
	//	//return CalculateDistance(m) == 2;
	//}
}