#include "HonourTile.h"

namespace MahJonggLib
{
	std::string HonourTile::GetName(const Honour&  honour)
	{
		return Honour::Name(honour);
	}

	pTile HonourTile::Create(const Honour&  honour)
	{
		return pTile(new HonourTile(honour));
	}
}