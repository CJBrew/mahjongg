#pragma once

#include <algorithm>

template < typename FwdIt, typename Predicate, typename T >
void FindPairs(FwdIt begin, FwdIt end, Predicate Pred, std::back_insert_iterator<std::vector<T>> result)
{
	FwdIt start = begin;
	while(start != end)
	{
		int pos = distance(begin, start);
		FwdIt pairIt = std::adjacent_find(start, end, Pred);
		if(pairIt == end)
		{
			// no (more) matching pairs found
			return;
		}
		else
		{
			result = T(pairIt, pairIt+1);			
		}
		start = pairIt + 1;
	}
}

