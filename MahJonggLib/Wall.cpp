#include "Wall.h"
#include <algorithm>

using namespace std;
namespace MahJonggLib
{
	Wall::Wall(const TileSet& tiles)
	{
		Tiles::const_iterator beg = tiles.begin();
		Tiles::const_iterator end = tiles.end();
		copy(beg, end, back_inserter(wall_));

		Shuffle();
	}

	void Wall::Shuffle()
	{
		std::random_shuffle(wall_.begin(), wall_.end());
	}

	bool Wall::HasTiles()
	{
		return wall_.size() > 14;
	}

	pTile Wall::GetNext()
	{
		if(HasTiles())
		{
			pTile tile = wall_.front();
			wall_.pop_front();
			return tile;
		}
		else
		{
			throw std::exception("Out of tiles");
		}
	}

	pTile Wall::GetLoose()
	{
		if(HasTiles())
		{
			pTile tile = wall_.back();
			wall_.pop_back();
			return tile;
		}
		else
		{
			throw std::exception("Out of tiles");
		}
	}
}