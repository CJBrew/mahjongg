#include "Tile.h"

#include "SuitTile.h"
#include "HonourTile.h"

#include <boost\format.hpp>

namespace MahJonggLib
{	
	SuitTile* AsSuitTile(pTile tile)
	{		
		if(tile->IsSuitTile())
		{
			return static_cast<SuitTile*>(tile.get());
		}
		else
		{
			throw std::bad_cast();
		}
	}

	HonourTile* AsHonourTile(pTile tile)
	{
		if(tile->IsHonourTile())
		{
			return static_cast<HonourTile*>(tile.get());
		}
		else
		{
			throw std::bad_cast();
		}
	}
	/*
	bool AreSameTiles(const pTile& a, const pTile& b)
	{
		return a == b;
	}*/

	bool operator==(const pTile& a, const pTile& b)
	{
		return a->Name() == b->Name();
	}

	bool operator<(const pTile& a, const pTile& b)
	{
		if(a->IsSuitTile())
		{ 
			if(!b->IsSuitTile())
			{
				return true;
			}

			SuitTile* stA = AsSuitTile(a);
			SuitTile* stB = AsSuitTile(b);
			if(stA->GetSuit() == stB->GetSuit())
			{
				return stA->GetNumber() < stB->GetNumber();
			}
			else
			{
				return stA->GetSuit() < stB->GetSuit();
			}
		}
		else if(a->IsHonourTile())
		{
			if(!b->IsHonourTile())
			{
				return false;
			}

			HonourTile* stA = AsHonourTile(a);
			HonourTile* stB = AsHonourTile(b);
			
			return stA->GetHonour() < stB->GetHonour();			
		}
		else 
		{
			throw std::exception((boost::format("Can't compare %1% and %2%") % a->Name() % b->Name()).str().c_str());
		}
	}
}