#pragma once

#include <string>
#include <vector>
#include <memory>

#include "Suit.h"
#include "Honour.h"

namespace MahJonggLib
{
	class SuitTile;
	class HonourTile;

	class Tile
	{
		std::string name_;
	
	protected:
		void SetName(std::string name)
		{
			name_ = name;
		}
	
		Tile(const std::string& name) : name_(name)
		{}
	public:		
		virtual bool IsSuitTile() = 0;
		virtual bool IsHonourTile() = 0;

		std::string Name() const { return name_; }
	}; 

	typedef std::tr1::shared_ptr<Tile> pTile;

	SuitTile* AsSuitTile(pTile);
	HonourTile* AsHonourTile(pTile);

//	bool AreSameTiles(const pTile& a, const pTile& b);

	bool operator==(const pTile&, const pTile&);
	bool operator<(const pTile&, const pTile&);

	typedef std::vector<pTile> Tiles;
}
