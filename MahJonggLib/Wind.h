#pragma once

#include "HonourBase.h"
#include <string>
#include <vector>

namespace MahJonggLib
{
	struct Wind : public Honour_base
	{
	private:
		enum Wind_ { East_, South_, West_, North_ };

		Wind_ value_;

	public:		
		explicit Wind(Wind_ value) : value_(value)
		{}

		static HonourType East()
		{
			return HonourType(new Wind(East_));
		}

		static HonourType South()		
		{
			return HonourType(new Wind(South_));
		}

		static HonourType West()
		{
			return HonourType(new Wind(West_));
		}

		static HonourType North()
		{
			return HonourType(new Wind(North_));
		}			

	public:		
		std::string Name() const;

		static HonourTypes WindTypes();

		friend bool operator<( const Wind& a, const Wind& b);
		friend bool operator==( const Wind& a, const Wind& b);
	};		

}

