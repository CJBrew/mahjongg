#include "Honour.h"

using namespace std;

namespace MahJonggLib
{	
	bool operator==( const Honour& a, const Honour& b)
	{
		return a.value_ == b.value_;
	}

	bool operator<( const Honour& a, const Honour& b)
	{
		return a.value_ < b.value_;
	}

	std::string Honour::Name(const Honour honour)
	{
		switch(honour.value_)
		{
		case East_:
			return "East wind";
		case South_:
			return "South wind";
		case West_:
			return "West wind";
		case North_:
			return "North wind";
		case Red_:
			return "Red dragon";
		case Green_:
			return "Green dragon";
		case White_:
			return "White dragon";

		default:
			throw std::range_error("Invalid Honour!");
		}
	}

	vector<Honour> Honour::HonourTypes()
	{
		vector<Honour> honours;
		honours.push_back(Honour::EastWind());
		honours.push_back(Honour::SouthWind()); 
		honours.push_back(Honour::WestWind());
		honours.push_back(Honour::NorthWind()); 
		honours.push_back(Honour::RedDragon());
		honours.push_back(Honour::GreenDragon());
		honours.push_back(Honour::WhiteDragon());
		return honours;
		/*Honour honours[] = { Wind::East(), 
		Wind::South(), 
		Wind::West(), 
		Wind::North(), 
		Dragon::Red(), 
		Dragon::Green(), 
		Dragon::White() };*/

	}

}