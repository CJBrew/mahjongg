#pragma once

#include "Tile.h"
#include <set>
#include <algorithm>

namespace MahJonggLib
{
	class Hand
	{		
	public:
		typedef std::vector<pTile> HandContents;
		typedef Hand::HandContents::const_iterator HandContentsIterator;
	private:
		static const int HAND_SIZE_MINIMUM = 13;
		HandContents tiles_;
	public:
		explicit Hand(const pTile tiles[HAND_SIZE_MINIMUM]) : tiles_(tiles, tiles + HAND_SIZE_MINIMUM)
		{
			sort(tiles_.begin(), tiles_.end());
		}

		HandContentsIterator begin() const { return tiles_.begin(); }
		HandContentsIterator end() const { return tiles_.end(); }

		void Draw(const pTile& tile);		
		void Discard(const pTile& tile);
	};

}