#pragma once

#include <string>
#include <vector>

namespace MahJonggLib
{
	struct Suit
	{
	private:
		enum Suit_ { Coins_, Characters_, Bamboo_ };

		Suit_ value_;

	public:		
		explicit Suit(Suit_ value) : value_(value)
		{}

		static Suit Bamboo()
		{
			return Suit(Bamboo_);
		}
		static Suit Characters()		
		{
			return Suit(Characters_);
		}
		static Suit Coins()
		{
			return Suit(Coins_);
		}

		static std::string Name(const Suit);
		
		static std::vector<Suit> SuitTypes();

		friend bool operator<( const Suit& a, const Suit& b);
		friend bool operator==( const Suit& a, const Suit& b);
	};	
}