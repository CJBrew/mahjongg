#include "Wind.h"

using namespace std;

namespace MahJonggLib
{	
	bool operator==( const Wind& a, const Wind& b)
	{
		return a.value_ == b.value_;
	}

	bool operator<( const Wind& a, const Wind& b)
	{
		return a.value_ < b.value_;
	}

	std::string Wind::Name() const
	{
		switch(value_)
		{
		case East_:
			return "East wind";
		case South_:
			return "South wind";
		case West_:
			return "West wind";
		case North_:
			return "North wind";

		default:
			throw std::range_error("Invalid Wind!");
		}
	}

	Honour_base::HonourTypes Wind::WindTypes()
	{
		Honour_base::HonourTypes winds;
		winds.push_back(Wind::East());
		winds.push_back(Wind::South()); 
		winds.push_back(Wind::West());
		winds.push_back(Wind::North()); 
		return winds;		
	}

}