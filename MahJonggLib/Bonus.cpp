#include "Bonus.h"

namespace MahJonggLib
{		
	std::string Bonus::Name(const Bonus bonus)
	{
		switch(bonus.value_)
		{
		case Spring_:
			return "Seasons: Spring";
		case Summer_:
			return "Seasons: Summer";
		case Autumn_:
			return "Seasons: Autumn";
		case Winter_:
			return "Seasons: Winter";
		case Plum_:
			return "Flowers: Plum";
		case Chrysanthemum_:
			return "Flowers: Chrysanthemum";
		case Orchid_:
			return "Flowers: Orchid";
		case Bamboo_:
			return "Flowers: Bamboo";

		default:
			throw std::range_error("Invalid Bonus!");
		}
	}
}