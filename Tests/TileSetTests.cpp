#include "..\MahJonggLib\TileSet.h"
#include "..\MahJonggLib\SuitTile.h"
#include "..\MahJonggLib\HonourTile.h"
#include "..\MahJonggLib\TilePredicates.h"

#include <msclr\marshal_cppstd.h>

#include <string>
#include <map>
#include <algorithm>

using namespace msclr::interop;

using namespace std;
using namespace System;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace MahJonggLib;

namespace
{	
	String^ Marshall(const string s) 
	{
		return marshal_as<String^>(s);
	}

	bool IsBamboo(pTile t)
	{			
		if(!t->IsSuitTile()) return false;

		return AsSuitTile(t)->GetSuit() == Suit::Bamboo();
	}

	bool IsCoins(pTile t)
	{			
		if(!t->IsSuitTile()) return false;

		return AsSuitTile(t)->GetSuit() == Suit::Coins();		
	}

	bool IsCharacters(pTile t)
	{			
		if(!t->IsSuitTile()) return false;

		return AsSuitTile(t)->GetSuit() == Suit::Characters();		
	}

	bool IsHonour(pTile t)
	{
		return t->IsHonourTile();
	}

	bool IsSuit(pTile t)
	{
		return t->IsSuitTile();
	}
	struct CountFour
	{			
	private:
		Tiles tiles_;
	public:
		CountFour(const Tiles& tiles) : tiles_(tiles)
		{}

		void operator()(Honour h)
		{
			Assert::IsTrue(4 == std::count_if(tiles_.begin(), tiles_.end(), IsSameHonour(h)));
		}
	};

	void TestHonours(Tiles honours)
	{
		Assert::IsTrue(honours.size() == (4 * 7));
		sort(honours.begin(), honours.end());
		map<Honour, int> numberPerHonour;

		for(Tiles::iterator it = honours.begin(); it < honours.end(); ++it)
		{
			HonourTile* pHt = static_cast<HonourTile*>(it->get());
			HonourTile& ht(*pHt);

			++numberPerHonour[ht.GetHonour()];
		}

		const vector<Honour> honourTypes = Honour::HonourTypes();
		for_each(honourTypes.begin(), honourTypes.end(), CountFour(honours));
	}

	void TestSuitTiles(Tiles suit)
	{
		Assert::IsTrue(suit.size() == (4 * 9));
		sort(suit.begin(), suit.end());
		map<int, int> numberPerNumber;

		for(Tiles::iterator it = suit.begin(); it < suit.end(); ++it)
		{
			SuitTile* pSt = static_cast<SuitTile*>(it->get());
			SuitTile& st(*pSt);
			++numberPerNumber[st.GetNumber()];
		}

		for(int i = 1; i <= 9; ++i)
		{
			Assert::IsTrue(numberPerNumber[i] == 4);
		}
	}

}

namespace Tests
{
	[TestClass]
	public ref class TileSetTest
	{

		void TestAllSuitTiles(Tiles suitTiles)
		{
			const vector<Suit> suitTypes = Suit::SuitTypes();
			Assert::IsTrue(suitTypes.size() == 3);

			Tiles::iterator bambooEnd = std::partition(suitTiles.begin(), suitTiles.end(), IsBamboo);
			Tiles::iterator coinsEnd = std::partition(bambooEnd, suitTiles.end(), IsCoins);
			Tiles::iterator characterEnd = std::partition(coinsEnd, suitTiles.end(), IsCharacters);

			TestSuitTiles(Tiles(suitTiles.begin(), bambooEnd));
			TestSuitTiles(Tiles(bambooEnd, coinsEnd));
			TestSuitTiles(Tiles(coinsEnd, suitTiles.end()));
		}

	public:

		[TestMethod]
		void TestTileSetCreation136Tiles()
		{
			try
			{
				TileSet ts = TileSet::CreateTileset_136();
				int count = ts.end() - ts.begin();
				Assert::IsTrue(count == 136);			
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};

		[TestMethod]
		void TestTileSetCreation136TestCharacters()
		{
			try
			{
				TileSet ts = TileSet::CreateTileset_136();
				Tiles tiles(ts.begin(), ts.end());
				Tiles suitTiles(tiles.begin(), std::partition(tiles.begin(), tiles.end(), IsCharacters));

				TestSuitTiles(suitTiles);
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		}

		[TestMethod]
		void TestTileSetCreation136TestCoins()
		{
			try
			{
				TileSet ts = TileSet::CreateTileset_136();
				Tiles tiles(ts.begin(), ts.end());
				Tiles suitTiles(tiles.begin(), std::partition(tiles.begin(), tiles.end(), IsCoins));

				TestSuitTiles(suitTiles);
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		}

		[TestMethod]
		void TestTileSetCreation136TestBamboo()
		{
			try
			{
				TileSet ts = TileSet::CreateTileset_136();
				Tiles tiles(ts.begin(), ts.end());
				
				Tiles suitTiles(tiles.begin(), std::partition(tiles.begin(), tiles.end(), IsSuit));
				Assert::IsTrue(suitTiles.size() == 108);				
				TestAllSuitTiles(suitTiles);

				Tiles honourTiles(tiles.begin(), std::partition(tiles.begin(), tiles.end(), IsHonour));
				Assert::IsTrue(honourTiles.size() == 28);
				TestHonours(honourTiles);
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		}
	};
}
