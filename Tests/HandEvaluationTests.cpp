#include "..\MahJonggLib\HandEvaluation.h"
#include "..\MahJonggLib\SuitTile.h"
#include "..\MahJonggLib\HonourTile.h"

#include <msclr\marshal_cppstd.h>

#include <string>
#include <algorithm>

using namespace msclr::interop;

using namespace std;
using namespace System;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace MahJonggLib;

namespace
{	
	String^ Marshall(const string s) 
	{
		return marshal_as<String^>(s);
	}
}

namespace Tests
{
	[TestClass]
	public ref class HandEvaluationTest
	{
		Hand CreateIncompleteHand()
		{
			// 13 tiles
			pTile tiles[13] = {SuitTile::Create(Suit::Characters(), 2),
				SuitTile::Create(Suit::Characters(), 2),
				SuitTile::Create(Suit::Characters(), 2),
				SuitTile::Create(Suit::Characters(), 4),
				SuitTile::Create(Suit::Characters(), 4),
				SuitTile::Create(Suit::Characters(), 4),
				SuitTile::Create(Suit::Coins(), 1),
				SuitTile::Create(Suit::Coins(), 1),
				SuitTile::Create(Suit::Coins(), 1),
				SuitTile::Create(Suit::Bamboo(), 2),
				SuitTile::Create(Suit::Bamboo(), 2),
				SuitTile::Create(Suit::Bamboo(), 2),
				SuitTile::Create(Suit::Characters(), 7)};
			Hand hand(tiles);			
			return hand;
		}

		Hand Create4MinorPungPairHand()
		{			
			pTile tiles[13] = {
				SuitTile::Create(Suit::Characters(), 2),
				SuitTile::Create(Suit::Characters(), 2),
				SuitTile::Create(Suit::Characters(), 2),
				SuitTile::Create(Suit::Characters(), 4),
				SuitTile::Create(Suit::Characters(), 4),
				SuitTile::Create(Suit::Characters(), 4),
				SuitTile::Create(Suit::Coins(), 1),
				SuitTile::Create(Suit::Coins(), 1),
				SuitTile::Create(Suit::Coins(), 1),
				SuitTile::Create(Suit::Bamboo(), 2),
				SuitTile::Create(Suit::Bamboo(), 2),
				SuitTile::Create(Suit::Bamboo(), 2),
				SuitTile::Create(Suit::Characters(), 7) };

				Hand hand(tiles);
				// final tile must be 'drawn'
				hand.Draw(SuitTile::Create(Suit::Characters(), 7));
				return hand;
		}

		Hand Create4ChowPairHand()
		{
			pTile tiles[13] = {
				SuitTile::Create(Suit::Characters(), 2),
				SuitTile::Create(Suit::Characters(), 3),
				SuitTile::Create(Suit::Characters(), 4),
				SuitTile::Create(Suit::Characters(), 7),
				SuitTile::Create(Suit::Characters(), 8),
				SuitTile::Create(Suit::Characters(), 9),			
				SuitTile::Create(Suit::Coins(), 1),
				SuitTile::Create(Suit::Coins(), 2),
				SuitTile::Create(Suit::Coins(), 3),
				SuitTile::Create(Suit::Bamboo(), 1),
				SuitTile::Create(Suit::Bamboo(), 2),
				SuitTile::Create(Suit::Bamboo(), 3),
				SuitTile::Create(Suit::Characters(), 7) };		
				Hand hand(tiles);		
				// final tile must be 'drawn'
				hand.Draw(SuitTile::Create(Suit::Characters(), 7));
				// interesting as this constitutes a pung 7wan or a pair with a 789wan chow
				return hand;
		}

	public:
		[TestMethod]
		void TestIsMahJongg_13tiles()
		{
			try
			{
				const Hand hand(CreateIncompleteHand());
				const HandEvaluation handEvaluation(hand);
				Assert::IsFalse(handEvaluation.IsMahJongg());
			}			
			catch(const std::exception& ex)
			{
				throw gcnew System::Exception(Marshall(ex.what()));
			}
		}
		[TestMethod]
		void TestIsMahJongg_4minorPung()
		{
			try
			{
				const Hand hand(Create4MinorPungPairHand());
				const HandEvaluation handEvaluation(hand);
				Assert::IsTrue(handEvaluation.IsMahJongg());
			}			
			catch(const std::exception& ex)
			{
				throw gcnew System::Exception(Marshall(ex.what()));
			}
		}

		[TestMethod]
		void TestEval_4ChowsPair()
		{
			/*
			SuitTile::Create(Suit::Characters(), 2),
				SuitTile::Create(Suit::Characters(), 3),
				SuitTile::Create(Suit::Characters(), 4),
				SuitTile::Create(Suit::Characters(), 7),
				SuitTile::Create(Suit::Characters(), 8),
				SuitTile::Create(Suit::Characters(), 9),			
				SuitTile::Create(Suit::Coins(), 1),
				SuitTile::Create(Suit::Coins(), 2),
				SuitTile::Create(Suit::Coins(), 3),
				SuitTile::Create(Suit::Bamboo(), 1),
				SuitTile::Create(Suit::Bamboo(), 2),
				SuitTile::Create(Suit::Bamboo(), 3),
				SuitTile::Create(Suit::Characters(), 7) };						
				SuitTile::Create(Suit::Characters(), 7));
				*/
			try
			{
				const Hand hand(Create4ChowPairHand());
				const HandEvaluation handEvaluation(hand);
				const HandEvalContentS possibles = handEvaluation.CalculatePossibleContents();
				for(HandEvalContentS::const_iterator it = possibles.begin(); it != possibles.end(); ++it)
				{
					const HandEvalContent& rit(*it);
					//rit.melds_

				}
				Assert::Fail();
			}
			catch(const std::exception& ex)
			{
				throw gcnew System::Exception(Marshall(ex.what()));
			}
		}

		[TestMethod]
		void TestIsMahJongg_4ChowsPair()
		{
			try
			{
				const Hand hand(Create4ChowPairHand());
				const HandEvaluation handEvaluation(hand);
				Assert::IsTrue(handEvaluation.IsMahJongg());
			}
			catch(const std::exception& ex)
			{
				throw gcnew System::Exception(Marshall(ex.what()));
			}
		}
	};
}