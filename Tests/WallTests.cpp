#include "..\MahJonggLib\Wall.h"
/*#include "..\MahJonggLib\SuitTile.h"
#include "..\MahJonggLib\HonourTile.h"
#include "..\MahJonggLib\TilePredicates.h"
*/

#include <msclr\marshal_cppstd.h>

#include <string>
//#include <map>
#include <algorithm>

using namespace msclr::interop;

using namespace std;
using namespace System;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace MahJonggLib;

namespace
{	
	String^ Marshall(const string s) 
	{
		return marshal_as<String^>(s);
	}

}

namespace Tests
{
	[TestClass]
	public ref class WallTest
	{
	public:
		[TestMethod]
		void TestWall136()
		{
			try
			{								
				Wall wall(TileSet::CreateTileset_136());

				int count;
				while(wall.HasTiles())
				{
					string name = wall.GetNext()->Name();
					System::Console::WriteLine(Marshall(name));
					++count;
				}
				Assert::IsTrue(count == 122);
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		}
	};
}
