#include "..\MahJonggLib\Meld.h"
#include "..\MahJonggLib\SuitTile.h"
//#include "..\MahJonggLib\Suit.h"
//#include "..\MahJonggLib\HonourTile.h"
//#include "..\MahJonggLib\Honour.h"

#include <msclr\marshal_cppstd.h>

#include <string>

using namespace msclr::interop;

using namespace std;
using namespace System;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace MahJonggLib;

namespace
{	
	String^ Marshall(const string s) 
	{
		return marshal_as<String^>(s);
	}
}

namespace Tests
{
	[TestClass]
	public ref class MeldsTest
	{
	public:

		[TestMethod]
		void TestValidMeld4()
		{
			try
			{
				Tiles tiles;
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				Meld meld(tiles.begin(), tiles.end());
				CheckMeld(meld);
				
			}
			catch(const exception&)
			{
				Assert::Fail("CheckMeld should not throw!");
			}
		};

		[TestMethod]
		void TestValidMeld3()
		{
			try
			{
				Tiles tiles;
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				Meld meld(tiles.begin(), tiles.end());
				CheckMeld(meld);
				
			}
			catch(const exception&)
			{
				Assert::Fail("CheckMeld should not throw!");
			}
		};

		[TestMethod]
		void TestInvalidMeld5()
		{
			try
			{
				Tiles tiles;
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				
				Meld meld(tiles.begin(), tiles.end());
				CheckMeld(meld);
				Assert::Fail("CheckMeld should throw!");
			}
			catch(const exception& ex)
			{
				Assert::AreEqual("Invalid Meld", Marshall(ex.what()));				
			}
		};

		[TestMethod]
		void TestInvalidMeld2()
		{
			try
			{
				Tiles tiles;
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				Meld meld(tiles.begin(), tiles.end());
				CheckMeld(meld);
				Assert::Fail("CheckMeld should throw!");
			}
			catch(const exception& ex)
			{
				Assert::AreEqual("Invalid Meld", Marshall(ex.what()));				
			}
		};

		[TestMethod]
		void TestInvalidMeld()
		{
			try
			{
				Tiles tiles;
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 1));
				tiles.push_back(SuitTile::Create(Suit::Bamboo(), 2));
				Meld meld(tiles.begin(), tiles.end());
				CheckMeld(meld);
				Assert::Fail("CheckMeld should throw!");
			}
			catch(const exception& ex)
			{
				Assert::AreEqual("Invalid Meld", Marshall(ex.what()));				
			}
		};

	};
}
