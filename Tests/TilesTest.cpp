#include "..\MahJonggLib\SuitTile.h"
#include "..\MahJonggLib\Suit.h"
#include "..\MahJonggLib\HonourTile.h"
#include "..\MahJonggLib\Honour.h"

#include <msclr\marshal_cppstd.h>

#include <string>

using namespace msclr::interop;

using namespace std;
using namespace System;
using namespace	Microsoft::VisualStudio::TestTools::UnitTesting;
using namespace MahJonggLib;

namespace
{	
	String^ Marshall(const string s) 
	{
		return marshal_as<String^>(s);
	}
}

namespace Tests
{
	[TestClass]
	public ref class TilesTest
	{
	public:

		[TestMethod]
		void TestEquality()
		{
			try
			{
				pTile tile1 = SuitTile::Create(Suit::Bamboo(), 1);
				pTile tile2 = SuitTile::Create(Suit::Bamboo(), 1);
				Assert::IsTrue(tile1 == tile2);
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};


		[TestMethod]
		void TestCreateBamboo()
		{
			try
			{
				pTile tile = SuitTile::Create(Suit::Bamboo(), 1);

				Assert::IsTrue(tile->Name() == "1 Bamboo");
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};


		[TestMethod]
		void TestCreateCoins()
		{
			try
			{
				pTile tile = SuitTile::Create(Suit::Coins(), 1);

				Assert::IsTrue(tile->Name() == "1 Coins");			
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};

		[TestMethod]
		void TestCreateCharacters()
		{
			try
			{
				pTile tile = SuitTile::Create(Suit::Characters(), 1);

				Assert::IsTrue(tile->Name() == "1 Characters");			
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};

		[TestMethod]
		void TestCreateSuitNumberRangeValid()
		{
			try
			{
				for(int i = 1; i <= 9; ++i)
				{
					SuitTile::Create(Suit::Characters(), i);
				}
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};

		[TestMethod]
		void TestCreateSuitNumberRangeInvalid()
		{
			try
			{
				pTile tile = SuitTile::Create(Suit::Characters(), 0);
				Assert::Fail();
			}
			catch(const exception&)
			{			
			}

			try
			{
				pTile tile = SuitTile::Create(Suit::Characters(), 10);
				Assert::Fail();
			}
			catch(const exception&)
			{			
			}
		};

		[TestMethod]
		void TestCreateDragonsWhite()
		{
			try
			{
				pTile tile = HonourTile::Create(Honour::WhiteDragon());

				Assert::IsTrue(tile->Name() == "White dragon");
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};

		[TestMethod]
		void TestCreateDragonsGreen()
		{
			try
			{
				pTile tile = HonourTile::Create(Honour::GreenDragon());

				Assert::IsTrue(tile->Name() == "Green dragon");
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};

		[TestMethod]
		void TestCreateDragonsRed()
		{
			try
			{
				pTile tile = HonourTile::Create(Honour::RedDragon());

				Assert::IsTrue(tile->Name() == "Red dragon");
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};

		[TestMethod]
		void TestCreateWindsEast()
		{
			try
			{
				pTile tile = HonourTile::Create(Honour::EastWind());

				Assert::IsTrue(tile->Name() == "East wind");			
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};

		[TestMethod]
		void TestCreateWindsSouth()
		{
			try
			{
				pTile tile = HonourTile::Create(Honour::SouthWind());

				Assert::IsTrue(tile->Name() == "South wind");			
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};

		[TestMethod]
		void TestCreateWindsWest()
		{
			try
			{
				pTile tile = HonourTile::Create(Honour::WestWind());

				Assert::IsTrue(tile->Name() == "West wind");			
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};

		[TestMethod]
		void TestCreateWindsNorth()
		{
			try
			{
				pTile tile = HonourTile::Create(Honour::NorthWind());

				Assert::IsTrue(tile->Name() == "North wind");			
			}
			catch(const exception& ex)
			{
				throw gcnew Exception(Marshall(string(ex.what())));
			}
		};
	};
}
